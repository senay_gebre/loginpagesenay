import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_app/modules/signup_page.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var a = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          bottom: false,
          child: Stack(
            children: [
              SvgPicture.asset(
                'images/Background.svg',
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                fit: BoxFit.fill,
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // fdgdfgfdg

                    Container(
                      padding: EdgeInsets.all(25),
                      child: Column(
                        children: [
                          // Column(
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: <Widget>[
                          //     SizedBox(
                          //       height: 50,
                          //     ),
                          //     Text(
                          //       "Welcome,",
                          //       style: TextStyle(
                          //           fontSize: 26, fontWeight: FontWeight.bold),
                          //     ),
                          //     SizedBox(
                          //       height: 6,
                          //     ),
                          //     Text(
                          //       "Sign in to continue!",
                          //       style: TextStyle(
                          //           fontSize: 20, color: Colors.grey.shade400),
                          //     ),
                          //   ],
                          // ),
                          //
                          SizedBox(
                            height: 90,
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 40)),
                          Column(
                            children: <Widget>[
                              TextField(
                                decoration: InputDecoration(
                                  labelText: "Username",
                                  labelStyle: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey.shade600),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                      color: Colors.grey.shade600,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                        color: Color(0xff000000),
                                      )),
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              TextField(
                                obscureText: true,
                                decoration: InputDecoration(
                                  labelText: "Password",
                                  labelStyle: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey.shade600),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                      color: Colors.grey.shade600,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                        color: Color(0xff000000),
                                      )),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              "Forgot Password ?",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff2991d7)),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            child: Column(
                              children: [
                                Container(
                                  height: 60,
                                  width: double.infinity,
                                  child: TextButton(
                                    onPressed: () {},
                                    child: Ink(
                                      height: 45,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(6),
                                        // gradient: LinearGradient(
                                        //   begin: Alignment.centerLeft,
                                        //   end: Alignment.centerRight,
                                        //   colors: [
                                        //     Color(0xffff5f6d),
                                        //     Color(0xffff5f6d),
                                        //     Color(0xffffc371),
                                        //   ],
                                        // ),
                                        color: Color(0xff1E85CA),
                                      ),
                                      child: Container(
                                        alignment: Alignment.center,
                                        constraints: BoxConstraints(
                                            maxWidth: double.infinity,
                                            minHeight: 50),
                                        child: Text(
                                          "LOGIN",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "   Don't have an account? ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                            return SignupPage();
                                          }));
                                        },
                                        child: Text(
                                          "Sign up",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xff3da7ed)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 60,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Stack(
                    //   children: [
                    //     Container(
                    //       width: double.infinity,
                    //       height: 160,
                    //       child: SvgPicture.asset(
                    //         'images/parent_login_footer_image.svg',
                    //         fit: BoxFit.fill,
                    //         // width: double.infinity,
                    //       ),
                    //     ),
                    //     Padding(
                    //       padding: const EdgeInsets.only(bottom: 27),
                    //       child: Container(
                    //         alignment: Alignment.topRight,
                    //         margin: EdgeInsets.only(right: 90,),
                    //         child: SvgPicture.asset(
                    //           'images/footer_key_icon.svg',
                    //           fit: BoxFit.fill,
                    //           // width: double.infinity,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
